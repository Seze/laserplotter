# Laser Plotter
<img src="Pictures/machine_overview.png" alt="drawing" width="750"/>

## THIS IS WORK IN PROGRESS

## Content
* Description
* Upcoming Features
* Firmware
* CAD
* Bill Of Materials

## Description
This is a simple laser-plotter intended to engrave wood and paper and cut adhesive materials such as stencil film.

## Upcoming Features
* Complete CAD Model (including all screws, nuts, washers, etc.) 
* BOM in CAD
* Complete Rewite of the firmware

## Firmware
This machine comes with its own firmware to enable machine specific features in the future.

## CAD
The whole machine has been modeled in Free-CAD. That includes all 3d printed files, metal parts and some of the electronic parts. Some dimensions that are shared between parts (for example the diameter of a snug fitting T8 rod hole) have been moved to a speadsheet to facilitate usage between sketches aswell as enabling easy modification of the machine. Most CAD parts have a their color set to a partial transparent one. This aids in modeling as well as identifying features and parts that may be on the inside.

## Bill Of Materials
* Printed Parts:
    * 2x LE6_XAxis_Carriage
    * 2x LE6_YAxis_Front
    * 2x LE6_YAxis_End
    * 1x LE6_Tool
    * 1x LE6_Box_Base
    * 1x LE6_Box_Lid
    * 1x LE6_Frame_KeyBox
    * Nx LE6_Frame_CableClip
* Metal Parts:
    * 8x 500mm Linear Rods 8mm Diameter
    * 3x 500mm Linear Screws T8 2mm Pitch
    * 4x Unloaded T8 Nut 2mm Pitch
    * 1x Preloaded T8 Nut 2mm Pitch
    * 8x Linear Bearing RJ4JP for 8mm Rods
    * 6x Ball Bearings for T8 with bottom holes
    * 3x T8 Shaft Coupler
* Screws
    * 8x  M4x16mm Screws and Nuts
    * 30x M4x10mm Screws, T-Nuts and Washers
    * 20x M3x16mm Screws, Nuts and Washers
    * 12x M3x10mm Screws and Washers´
* Electronics:
    * 1x Creality Ender3 Mainboard with 8Bit AVR processor
    * 3x Nema 17 Stepper Motors
    * 3x Endstop Switches 
    * 1x Keyed Switch
    * 1x 5Pin DIN Connector
    * 1x 3W Laser, Not wider than 40mm
#include "Arduino.h"

#include "include/Driver.h"
#include "include/Endstop.h"
#include "include/gcode/gcode.h"
#include "include/action/Homing.h"
#include "include/action/Kinematic.h"

#define SERIAL_BUFFER_SIZE 200

uint8_t serialLineOffset;
char serialLineBuffer[SERIAL_BUFFER_SIZE];

void setup() {
	Serial.begin(115200);
	Serial.println("HOLA");

	initEndstopModule();
	initStepperDriverModule();

	pinMode(FAN_PIN, OUTPUT);
	pinMode(LASER_PIN, OUTPUT);
	setPDMLevel(OUT_PDM_LASER, LASER_INVERT ? 32767 : 0);

	// This lets everything settle and get nice and stable before we actually start doing something
	setStepperEnable(0, false);
	setStepperEnable(1, false);
	setStepperEnable(2, false);
	setStepperEnable(3, false);
	delay(2000);
	setStepperEnable(0, true);
	setStepperEnable(1, true);
	setStepperEnable(2, true);
	setStepperEnable(3, true);

	// homeAllAxis();
}

uint32_t lastPosPrintTime = millis();

void loop() {

	if (millis() - lastPosPrintTime > 500) {
		lastPosPrintTime = millis();
		Serial.print("X: ");
		Serial.print(getStepperPosition(KINEMATIC_STEPPER_X) / (float) DRIVER_STEPS_PER_MM);
		Serial.print(" Y0: ");
		Serial.print(getStepperPosition(KINEMATIC_STEPPER_Y0) / (float) DRIVER_STEPS_PER_MM);
		Serial.print(" Y1: ");
		Serial.println(getStepperPosition(KINEMATIC_STEPPER_Y1) / (float) DRIVER_STEPS_PER_MM);
	}

	// Parse anything that is passed through serial
	if (Serial.available()) {
		while (serialLineOffset < SERIAL_BUFFER_SIZE && Serial.available()) {
			char chr = Serial.read();
			serialLineBuffer[serialLineOffset++] = chr;
			if (chr < 0 || chr == '\n') break;
		}
		if (serialLineBuffer[serialLineOffset - 1] == '\n') {
			if (strncmp(serialLineBuffer, "PRINT", 5) == 0) {
				while (gcode_available()) {
					gcode_instruction_str inst = _gcode_read(0);
					Serial.print(inst.code);
					Serial.print(" ");
					Serial.print(inst.codeArgs.codePoint);
					Serial.print(" ");
					Serial.print(inst.codeArgs.length);
					Serial.print(" -> ");
					Serial.println(inst.argument);
					_gcode_clear(1);
				}
				serialLineOffset = 0;
			} else if (strncmp(serialLineBuffer, "RUN", 3) == 0) {
				while (gcode_available()) {
					executeNextGInstruction();
				}
				serialLineOffset = 0;
			} else {
				// Submit line to parser
				parseGCodeLine(serialLineBuffer);
				serialLineOffset = 0;
			}
		}
	}

}

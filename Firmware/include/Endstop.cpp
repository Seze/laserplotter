/*
 * Endstop.cpp
 *
 *  Created on: 23.06.2020
 *      Author: seze
 */

#include "Endstop.h"

uint8_t ENDSTOP_PINS[] = { PIN_X_STOP, PIN_Y_STOP, PIN_Z_STOP};

void initEndstopModule() {
	for (uint8_t pidx = 0; pidx < _ENDSTOP_NUM_ENDSTOP; pidx++) {
		pinMode(ENDSTOP_PINS[pidx], INPUT);
	}
}

bool getEndstopState(uint8_t endstop) {
	return digitalRead(ENDSTOP_PINS[endstop]);
}


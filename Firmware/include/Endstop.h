/*
 * Endstop.h
 *
 *  Created on: 23.06.2020
 *      Author: seze
 */

#pragma once

// TODO: HAL
#include <Arduino.h>
#include <stdint.h>
#include "Config.h"

#define _ENDSTOP_NUM_ENDSTOP 3

extern uint8_t ENDSTOP_PINS[];

void initEndstopModule();
bool getEndstopState(uint8_t endstop);

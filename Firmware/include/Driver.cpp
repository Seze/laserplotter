/*
 * Driver.cpp
 *
 *  Created on: 22.06.2020
 *      Author: seze
 */

#include "Driver.h"

volatile uint8_t stepperDriverState = DRIVER_ENUM_STATE::HALTED;
volatile stepper_str DRIVER_STEPPER[] = {
		// ENABLE, DIR, STEP, F_MODE, F_REACHED, F_DIR, SPEED_TGT, SPEED_ERR, POS_CUR, POS_TGT
		{X_ENABLE_PIN,  X_DIR_PIN,  X_STEP_PIN,  0, 0, 0, 0, 0, 0, 0}, // PortX Stepper
		{Y_ENABLE_PIN,  Y_DIR_PIN,  Y_STEP_PIN,  0, 0, 0, 0, 0, 0, 0}, // PortY Stepper
		{Z_ENABLE_PIN,  Z_DIR_PIN,  Z_STEP_PIN,  0, 0, 0, 0, 0, 0, 0}, // PortZ Stepper
		{E0_ENABLE_PIN, E0_DIR_PIN, E0_STEP_PIN, 0, 0, 0, 0, 0, 0, 0}  // PortE Stepper
};

volatile pdmpin_str PDM_PIN[] = {
		{LASER_PIN, LASER_INVERT, 0, 0}	// Laser output pin
};

ISR(TIMER1_COMPA_vect) {

	// PDM modulation on IO pin
	for (uint8_t pidx = 0; pidx < _DRIVER_NUM_PDMPIN; pidx++) {
		bool active = (PDM_PIN[pidx].target >= PDM_PIN[pidx].error);
		digitalWrite(PDM_PIN[pidx].pin, active);
		int16_t level = active ? 32767 : -32767;
		PDM_PIN[pidx].error = level - PDM_PIN[pidx].target + PDM_PIN[pidx].error;
	}

	// Stepper driver PDM modulation
	if (stepperDriverState == RUNNING) {
		for (uint8_t sidx = 0; sidx < _DRIVER_NUM_STEPPER; sidx++) {

			// Disable the stepper if position ctrl is enabled and target is reached
			if (DRIVER_STEPPER[sidx].flagUsePositionCtrl &&
				DRIVER_STEPPER[sidx].position == DRIVER_STEPPER[sidx].targetPosition) {
				DRIVER_STEPPER[sidx].flagSetPositionReached = true;
				DRIVER_STEPPER[sidx].targetSpeed = 0;
				// As speed is set to zero, this will stop the next iteration of the loop from occurring
				DRIVER_STEPPER[sidx].speedError = 1;
			}

			// Output a pdm signal that matches the desired step-rate
			if (DRIVER_STEPPER[sidx].targetSpeed >= DRIVER_STEPPER[sidx].speedError) {

				// Signal the driver to take the next step
				// TODO: Direct port access
				digitalWrite(DRIVER_STEPPER[sidx].pinStep, HIGH);
				digitalWrite(DRIVER_STEPPER[sidx].pinStep, LOW);

				// Update the position of the stepper
				DRIVER_STEPPER[sidx].position += DRIVER_STEPPER[sidx].flagDirection ? 1 : -1; // TODO: remove branch

				// Update the velocity error term (positive step)
				DRIVER_STEPPER[sidx].speedError = 32767 - DRIVER_STEPPER[sidx].targetSpeed + DRIVER_STEPPER[sidx].speedError;

			} else {
				// Update the velocity error term (negative step)
				DRIVER_STEPPER[sidx].speedError = -DRIVER_STEPPER[sidx].targetSpeed + DRIVER_STEPPER[sidx].speedError;
			}
		}
	} else {
		for (uint8_t sidx = 0; sidx < _DRIVER_NUM_STEPPER; sidx++) {
			digitalWrite(DRIVER_STEPPER[sidx].pinEnable, HIGH);
		}
		stepperDriverState = HALTED;
	}
}

void initStepperDriverModule() {
	cli();
		TCCR1A = 0;                          // Clear Control register
		TCCR1B = 0;
		TCNT1  = 0;                          // Clear Counter register
		OCR1A = 16 * _DRIVER_INTERRUPT_TIME; // 1us * DRIVER_US_PER_STEP
		TCCR1B |= (1 << WGM12);	             // turn on CTC mode
		TCCR1B |= (1 << CS00); 	             // Set CS00 for 1 prescaler
		TIMSK1 |= (1 << OCIE1A);             // enable timer compare interrupt
	sei();

	for (uint8_t sidx = 0; sidx < _DRIVER_NUM_STEPPER; sidx++) {
		pinMode(DRIVER_STEPPER[sidx].pinDirection, OUTPUT);
		pinMode(DRIVER_STEPPER[sidx].pinEnable, OUTPUT);
		pinMode(DRIVER_STEPPER[sidx].pinStep, OUTPUT);
	}

	stepperDriverState = DRIVER_ENUM_STATE::RUNNING;
}

void setStepperEnable(uint8_t stepper, bool enable) {
	digitalWrite(DRIVER_STEPPER[stepper].pinEnable, enable ? LOW : HIGH);
}

void setStepperSpeed(uint8_t stepper, stepsPerSecond_t speed) {

	// Find the number of pulses per second needed to drive at the given speed
	// This depends on the speed and the update frequency of the ISR
	int32_t pulsesPerSecond = ((int32_t) speed << 15) / DRIVER_UPDATE_FREQUENCY;

	// Directly output the direction to the driver
	digitalWrite(DRIVER_STEPPER[stepper].pinDirection, speed < 0);

	// Setup the stepper for speed control
	DRIVER_STEPPER[stepper].flagUsePositionCtrl = false;
	DRIVER_STEPPER[stepper].flagDirection = speed > 0;
	DRIVER_STEPPER[stepper].targetSpeed = speed > 0 ? pulsesPerSecond : -pulsesPerSecond; // TODO: remove branch
	DRIVER_STEPPER[stepper].speedError = 0;

}

void setStepperPosition(uint8_t stepper, steps_t positionInSteps) {
	DRIVER_STEPPER[stepper].position = positionInSteps;
}

void setStepperTarget(uint8_t stepper, stepsPerSecond_t feed, steps_t position) {

	// Transfer the sign of the travel direction onto the feed-rate
	stepsPerSecond_t feedRate = position > DRIVER_STEPPER[stepper].position ? feed : -feed;

	// Setup stepper with the corrected feed-rate
	setStepperSpeed(stepper, feedRate);

	// Change stepper to use positional control and stop at the given position
	DRIVER_STEPPER[stepper].flagUsePositionCtrl = true;
	DRIVER_STEPPER[stepper].flagSetPositionReached = false;
	DRIVER_STEPPER[stepper].targetPosition = position;
}

bool hasStepperReachedTarget(uint8_t stepper) {
	return DRIVER_STEPPER[stepper].flagSetPositionReached;
}

steps_t getStepperPosition(uint8_t stepper) {
	return DRIVER_STEPPER[stepper].position;
}

// values between [0, 32767] (negative logic level output not yet supported)
void setPDMLevel(uint8_t pdmout, int16_t target) {
	PDM_PIN[pdmout].error = 0;
	PDM_PIN[pdmout].target = target;
}

/*
 * Driver.h
 *
 *  Created on: 22.06.2020
 *      Author: seze
 */

#pragma once

// TODO: HAL
#include "Arduino.h"

// #include <avr/iom1284p.h>
// #include <avr/interrupt.h>
#include <stdint.h>
#include "Config.h"
#include "Endstop.h"

#define _DRIVER_INTERRUPT_TIME (1e6 / DRIVER_UPDATE_FREQUENCY)
#define _DRIVER_NUM_STEPPER 4
#define _DRIVER_NUM_PDMPIN 1

#define OUT_PDM_LASER 0

typedef int16_t stepsPerSecond_t;
typedef int32_t steps_t;

struct stepper_str {

	// HW Setup
	uint8_t pinEnable;
	uint8_t pinDirection;
	uint8_t pinStep;

	// Drive mode flags
	uint8_t flagUsePositionCtrl : 1;
	uint8_t flagSetPositionReached : 1;
	uint8_t flagDirection : 1;

	// Velocity and Position data
	stepsPerSecond_t targetSpeed;
	stepsPerSecond_t speedError;
	steps_t position;
	steps_t targetPosition;
};

struct pdmpin_str {
	uint8_t pin;
	uint8_t invert;
	int16_t target;
	int16_t error;
};

enum DRIVER_ENUM_STATE : uint8_t {
	HALTED,
	RUNNING
};

extern volatile stepper_str DRIVER_STEPPER[];
extern volatile uint8_t stepperDriverState;

// ISR(TIMER1_COMPA_vect)
void initStepperDriverModule();
void setStepperEnable(uint8_t stepper, bool enable);
void setStepperSpeedRaw(uint8_t stepper, int16_t speed);
void setStepperSpeed(uint8_t stepper, stepsPerSecond_t speed);
void setStepperPosition(uint8_t stepper, steps_t positionInSteps);
void setStepperTarget(uint8_t stepper, stepsPerSecond_t feed, steps_t position);
bool hasStepperReachedTarget(uint8_t stepper);
int32_t getStepperPosition(uint8_t stepper);

void setPDMLevel(uint8_t pdmout, int16_t target);

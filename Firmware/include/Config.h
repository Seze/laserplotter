/*
 * Config.h
 *
 *  Created on: 22.06.2020
 *      Author: seze
 */

#pragma once

// Pin Setup

#define LCD_PINS_CS                   30  // CS chip select /SS chip slave select
#define LCD_PINS_MOSI                 29  // SID (MOSI)
#define LCD_PINS_SCK                  17  // SCK (CLK) clock
#define BEEPER_PIN                    27

// These are the original pins
// #define PIN_ETEMP 24
// #define PIN_BMOT  25
// #define PIN_END_Z 20

#define PIN_X_STOP 24
#define PIN_Y_STOP 25
#define PIN_Z_STOP 20

#define X_ENABLE_PIN                          14
#define X_STEP_PIN                            15
#define X_DIR_PIN                             21

#define Y_ENABLE_PIN                          14
#define Y_STEP_PIN                            22
#define Y_DIR_PIN                             23

#define Z_ENABLE_PIN                          26
#define Z_STEP_PIN                             3
#define Z_DIR_PIN                              2

#define E0_ENABLE_PIN                         14
#define E0_STEP_PIN                            1
#define E0_DIR_PIN                             0

#define HEATER_0_PIN                          13  // (extruder)
#define HEATER_BED_PIN                        12  // (bed)

#define LASER_PIN 							  18
#define LASER_INVERT					    true

#define FAN_PIN 							   4

// Stepper Driver Setup, See also Driver.h for Stepper Configuration

#define DRIVER_UPDATE_FREQUENCY 20000
#define DRIVER_STEPS_PER_MM 400

#define MM_TO_STEPS(mm) (mm * DRIVER_STEPS_PER_MM)
#define MMS_TO_STEPS(mms) (mms * DRIVER_STEPS_PER_MM)

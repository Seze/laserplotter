/*
 * Kinematic.h
 *
 *  Created on: Jun 26, 2020
 *      Author: seze
 */

#pragma once

#include <math.h>
#include "../Config.h"
#include "../Driver.h"

#define KINEMATIC_STEPPER_Y0 2
#define KINEMATIC_STEPPER_Y1 3
#define KINEMATIC_STEPPER_X  0

float getPositionX();
float getPositionY();
void linearMoveTo(float xInMM, float yInMM, float feedInMMS);
bool movementIsFinished();

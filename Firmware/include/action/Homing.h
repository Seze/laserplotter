/*
 * Homing.h
 *
 *  Created on: 23.06.2020
 *      Author: seze
 */

#pragma once

#include <stdint.h>
#include "../Config.h"
#include "../Driver.h"
#include "../Endstop.h"
#include "Kinematic.h"

#define HOMING_BACKOFF_TIME 2000
#define _HOMING_NUM_HOMEABLE 3

struct homeableStepper_str {
	stepsPerSecond_t fastSpeed;
	stepsPerSecond_t backoffSpeed;
	stepsPerSecond_t slowSpeed;
	steps_t homeOffset;
	uint8_t homeSwitch;
	uint8_t stepper;
};
extern homeableStepper_str HOMABLE_STEPPER[];

void homeAllAxis();

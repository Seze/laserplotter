/*
 * Homeing.cpp
 *
 *  Created on: 23.06.2020
 *      Author: seze
 */

#include "Homing.h"

homeableStepper_str HOMABLE_STEPPER[] = {
		// Fast, Backoff, Slow, Offset, Switch, Stepper
		{MMS_TO_STEPS(-15), MMS_TO_STEPS(5), MMS_TO_STEPS(-2), MM_TO_STEPS(0L), 0, KINEMATIC_STEPPER_Y0},
		{MMS_TO_STEPS(-15), MMS_TO_STEPS(5), MMS_TO_STEPS(-2), MM_TO_STEPS(0L), 1, KINEMATIC_STEPPER_Y1},
		{MMS_TO_STEPS(-15), MMS_TO_STEPS(5), MMS_TO_STEPS(-2), MM_TO_STEPS(0L), 2, KINEMATIC_STEPPER_X}
};

void homeAllAxis() {

	// This will contain the current state of the home
	/* 0 - Beginning home, backoff when home is closed, else move home fast
	 * 1 - Waiting for contact, backing of
	 * 2 - Waiting for backoff to be done, home slow
	 * 3 - Waiting for contact, homing done
	 * 4 - Done
	 */
	uint8_t homeState[_HOMING_NUM_HOMEABLE];
	for (uint8_t hidx = 0; hidx < _HOMING_NUM_HOMEABLE; homeState[hidx++] = 0);

	uint32_t backoffStartTime[_HOMING_NUM_HOMEABLE];
	uint8_t numUnhomed = _HOMING_NUM_HOMEABLE;

	// TODO: Use kinematic in speed control mode
	while (numUnhomed != 0) {
		for (uint8_t hidx = 0; hidx < _HOMING_NUM_HOMEABLE; hidx++) {
			homeableStepper_str context = HOMABLE_STEPPER[hidx];
			switch (homeState[hidx]) {
				case 0 : {
					if (getEndstopState(context.homeSwitch)) {
						setStepperSpeed(context.stepper, context.backoffSpeed);
						backoffStartTime[hidx] = millis();
						homeState[hidx] = 2;
					} else {
						setStepperSpeed(context.stepper, context.fastSpeed);
						homeState[hidx] = 1;
					}
				} break;
				case 1 : {
					if (getEndstopState(context.homeSwitch)) {
						setStepperSpeed(context.stepper, context.backoffSpeed);
						backoffStartTime[hidx] = millis();
						homeState[hidx] = 2;
					}
				} break;
				case 2 : {
					if (millis() - backoffStartTime[hidx] >= HOMING_BACKOFF_TIME) {
						setStepperSpeed(context.stepper, context.slowSpeed);
						homeState[hidx] = 3;
					}
				} break;
				case 3 : {
					if (getEndstopState(context.homeSwitch)) {
						setStepperSpeed(context.stepper, 0.0);
						numUnhomed -= 1;
						setStepperPosition(context.stepper, context.homeOffset);
						homeState[hidx] = 4;
					}
				} break;
				default: { } break;
			}
		}
	}

}

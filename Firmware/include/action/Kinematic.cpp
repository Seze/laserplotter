/*
 * Kinematic.cpp
 *
 *  Created on: Jun 26, 2020
 *      Author: seze
 */

#include "Kinematic.h"

float getPositionX() {
	return getStepperPosition(KINEMATIC_STEPPER_X) / (float) DRIVER_STEPS_PER_MM;
}

float getPositionY() {
	// TODO: This is not exactly true, but they should be the same none the less...
	return getStepperPosition(KINEMATIC_STEPPER_Y0) / (float) DRIVER_STEPS_PER_MM;
}

void linearMoveTo(float xInMM, float yInMM, float feedInMMS) {
	// TODO: excessive use of float math - use steps instead
	Serial.println("Executing linear move to");
	Serial.println(xInMM);
	Serial.println(yInMM);
	Serial.println(feedInMMS);

	float dx = xInMM - getPositionX();
	float dy = yInMM - getPositionY();
	float len = sqrt(dx * dx + dy * dy);

	cli();
		setStepperTarget(KINEMATIC_STEPPER_X,  (dx / len) * feedInMMS * (dx > 0 ? 1.0f : -1.0f) * DRIVER_STEPS_PER_MM, xInMM * DRIVER_STEPS_PER_MM);
		setStepperTarget(KINEMATIC_STEPPER_Y0, (dy / len) * feedInMMS * (dy > 0 ? 1.0f : -1.0f) * DRIVER_STEPS_PER_MM, yInMM * DRIVER_STEPS_PER_MM);
		setStepperTarget(KINEMATIC_STEPPER_Y1, (dy / len) * feedInMMS * (dy > 0 ? 1.0f : -1.0f) * DRIVER_STEPS_PER_MM, yInMM * DRIVER_STEPS_PER_MM);
	sei();
}

bool movementIsFinished() {
	return 	hasStepperReachedTarget(KINEMATIC_STEPPER_Y0) &&
			hasStepperReachedTarget(KINEMATIC_STEPPER_Y1) &&
			hasStepperReachedTarget(KINEMATIC_STEPPER_X);
}


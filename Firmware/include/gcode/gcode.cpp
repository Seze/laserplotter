/*
 * gcode.cpp
 *
 *  Created on: 03.07.2020
 *      Author: seze
 */

#include "gcode.h"

gcode_buffer_str gcode_buffer;
float FRACT10[] = {
		1.0f,
		0.1f,
		0.01f,
		0.001f,
		0.0001f,
		0.00001f,
		0.000001f,
		0.0000001f
};

void executeNextGInstruction() {

	// Read surrogate
	gcode_instruction_str instruction = _gcode_read(0);
	uint8_t clearSize = instruction.codeArgs.length;

	// TODO: Look up table?
	switch (instruction.code) {
		case 'G' : {
			switch (instruction.codeArgs.codePoint) {
				case 0 :
				case 1 : {
					uint8_t iLen = instruction.codeArgs.length;
					float x, y, s, f;
					_gcode_find_code_argument('X', iLen, &x, getPositionX());
					_gcode_find_code_argument('Y', iLen, &y, getPositionY());
					_gcode_find_code_argument('F', iLen, &f, 0.0f);
					_gcode_find_code_argument('S', iLen, &s, 600.0f);
					Serial.print("Linear move to ");
					Serial.print(x);
					Serial.print(" ");
					Serial.print(y);

					Serial.print(" Feed: ");
					Serial.print(f);
					Serial.print(" Speed: ");
					Serial.print(s);

					// Control laser output
					bool laserEnable = instruction.codeArgs.codePoint == 1;
					int16_t level = laserEnable ? f * 32767.0f : 0;
					level = LASER_INVERT ? 32767 - level : level;
					setPDMLevel(OUT_PDM_LASER, level);
					Serial.println(level);

					// TODO: Use of float division
					linearMoveTo(x, y, s / 60.0f);

					// TODO: This should yield() under a specific condition
					// while (!movementIsFinished()) { }
				} break;
				case 4 : {
					float p;
					_gcode_find_code_argument('P', instruction.codeArgs.length, &p, getPositionX());
					// TODO: This should yield() under a specific condition
					delay(p);
				} break;
				case 28 : {
					homeAllAxis();
				} break;
				default: { } break;
			}
		} break;
		case 'M' : {
			switch (instruction.codeArgs.codePoint) {
				case 5 : { } break;
				case 106 :
				case 107 : {
					float s;
					// TODO: clamp parameter s
					float defaultSpeed = instruction.code == 106 ? 255 : 0;
					_gcode_find_code_argument('S', instruction.codeArgs.length, &s, defaultSpeed);
					analogWrite(FAN_PIN, (int) s);
					Serial.println("FAN speed set");
				} break;
				default: { } break;
			}
		} break;
	}

	// Clear instruction from buffer
	_gcode_clear(clearSize + 1);

}

void parseGCodeLine(const char* line) {

	gcode_instruction_str* surrogate = nullptr;	// Pointer to the surogate atom (G0, G1, M4 etc.)
	uint8_t numParsedAtoms = 0;					// Number of parsed parameter atoms (X12, Y23, etc.)

	char parsingChar = '\0';	// Current atom letter (e.g. G, X, Y, etc.)
	bool nbrIsNegativ = false;	// Sign of the number, false indicates positive
	uint32_t nbrDigits = 0;		// The digits the number is composed of
	uint8_t nbrFractLen = 0;    // The length of the integer part of the number
	uint8_t nbrFractInc = 0;    // Integer part length increment, indicates if the number is whole

	// Find and parse instruction atoms, record number of atoms in first atom
	const char* chrp = line;
	while (*chrp) {
		char chr = *chrp++;

		// On whitespace, Submit parser state to buffer
		if ((chr == ' ' || chr == '\t' || chr == '\r' || chr == '\n') && parsingChar != '\0') {
			if (numParsedAtoms == 0) {
				uint8_t codepoint = nbrDigits;
				surrogate = _gcode_buffer_insert(parsingChar, codepoint);
			} else {
				float argument = nbrDigits * FRACT10[nbrFractLen]; // TODO : float math
				_gcode_buffer_insert(parsingChar, nbrIsNegativ ? -argument : argument);
			}
			numParsedAtoms += 1;
			parsingChar = '\0';
			nbrDigits = 0;
			nbrFractLen = 0;
			nbrFractInc = 0;
			nbrIsNegativ = false;
		}

		// Stop parser on line break
		if (chr == '\n') break;

		// Begin a new atom (letter followed by number)
		if (chr >= 'A' && chr <= 'Z') {
			parsingChar = chr;
		}

		// Parse the current digit into a int containing integral and fractional part
		if (chr >= '0' && chr <= '9') {
			nbrDigits = (nbrDigits << 3) + (nbrDigits << 1); // Multiply by 10
			nbrDigits += chr - '0'; 						 // Add current digit
			nbrFractLen += nbrFractInc;						 // Count the number of fractional digits
		}

		// On point, start parsing the fractional part of the argument
		if (chr == '.') {
			nbrFractInc = 1;
		}

		// On minus, declare the current number as negative
		if (chr == '-') {
			nbrIsNegativ = true;
		}

	}

	// Change the number of parsed atoms in the surrogate entry
	surrogate->codeArgs.length = numParsedAtoms - 1;

}

bool _gcode_find_code_argument(char code, uint8_t numArgs, float* arg, float orElse) {
	gcode_instruction_str inst;
	for (uint8_t offset = 1; offset <= numArgs; offset++) {
		inst = _gcode_read(offset);
		if (inst.code == code) {
			*arg = inst.argument;
			return true;
		}
	}
	*arg = orElse;
	return false;
}

uint16_t gcode_available() {
	return gcode_buffer.length;
}

gcode_instruction_str* _gcode_buffer_insert(uint8_t code, uint8_t codepoint) {
	uint16_t insertAt = gcode_buffer.write;
	gcode_instruction_str* entry = &(gcode_buffer.data[insertAt]);
	gcode_buffer.data[insertAt].code = code;
	gcode_buffer.data[insertAt].codeArgs.codePoint = codepoint;
	insertAt += 1; // TODO: rollover could be handled better for 2**n buffer size
	if (insertAt >= GCODE_PREPARSED_BUFFER_SIZE) insertAt = 0;
	gcode_buffer.write = insertAt;
	gcode_buffer.length += 1;
	return entry;
}

void _gcode_buffer_insert(uint8_t code, float argument) {
	uint16_t insertAt = gcode_buffer.write;
	gcode_buffer.data[insertAt].code = code;
	gcode_buffer.data[insertAt].argument = argument;
	insertAt += 1; // TODO: rollover could be handled better for 2**n buffer size
	if (insertAt >= GCODE_PREPARSED_BUFFER_SIZE) insertAt = 0;
	gcode_buffer.write = insertAt;
	gcode_buffer.length += 1;
}

gcode_instruction_str _gcode_read(uint16_t offset) {
	return gcode_buffer.data[(gcode_buffer.read + offset) % GCODE_PREPARSED_BUFFER_SIZE]; // TODO: use of mod
}

void _gcode_clear(uint16_t length) {
	gcode_buffer.read = (gcode_buffer.read + length) % GCODE_PREPARSED_BUFFER_SIZE; // TODO: use of mod
	gcode_buffer.length -= length;
}

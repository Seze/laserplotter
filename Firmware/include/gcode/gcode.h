/*
 * gcode.h
 *
 *  Created on: 03.07.2020
 *      Author: seze
 */

#pragma once

#include <Arduino.h>
#include <stdint.h>

#include "../action/Kinematic.h"
#include "../action/Homing.h"

// This takes about 512 * 5B = 2.5KB (2566B actual)
#define GCODE_PREPARSED_BUFFER_SIZE 512

#define GCODE_NAME_NULL  0
#define GCODE_NAME_X     1
#define GCODE_NAME_Y     2
#define GCODE_NAME_Z     3
#define GCODE_NAME_F     4
#define GCODE_NAME_S     5
#define GCODE_NAME_P     6
#define GCODE_NAME_G    32
#define GCODE_NAME_M   160

// Represents a gcode parameter (e.g. G1(2) X42.0 Y3.14 etc.)
struct gcode_instruction_str {
	char code;
	union {
		struct {
			uint32_t codePoint : 8;
			uint32_t length    : 8;
		} codeArgs;
		float argument; // TODO: change this to a fixed digit number
	};
};

struct gcode_buffer_str {
	uint16_t read;
	uint16_t write;
	uint16_t length;
	gcode_instruction_str data[GCODE_PREPARSED_BUFFER_SIZE];
};

extern gcode_buffer_str gcode_buffer;
extern float FRACT10[]; // TODO: move to math util

// Datastructure handling
gcode_instruction_str* _gcode_buffer_insert(uint8_t code, uint8_t codepoint);
void _gcode_buffer_insert(uint8_t code, float argument);
gcode_instruction_str _gcode_read(uint16_t offset);
void _gcode_clear(uint16_t length);
bool _gcode_find_code_argument(char code, uint8_t numArgs, float* arg, float orElse);

void parseGCodeLine(const char* line);
void executeNextGInstruction();
uint16_t gcode_available();

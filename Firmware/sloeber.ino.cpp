#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2020-07-26 21:00:12

#include "Arduino.h"
#include "Arduino.h"
#include "include/Driver.h"
#include "include/Endstop.h"
#include "include/gcode/gcode.h"
#include "include/action/Homing.h"
#include "include/action/Kinematic.h"

void setup() ;
void loop() ;


#include "LaserFirmware.ino"

#endif
